package com.example.androidproj;

import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.content.res.TypedArrayUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.androidproj.adaptor.HomeFragRecyclerAdaptor;
import com.example.androidproj.databinding.FragmentHomeBinding;
import com.example.androidproj.model.AnswerSheet;
import com.example.androidproj.model.Folder;
import com.example.androidproj.model.InMemoryDatabase;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class HomeFragment extends Fragment implements
        NewAnswerSheetDialogFragment.NewAnswerSheetDialogListener,
        NewFolderDialogFragment.NewFolderDialogListener{

    List<Integer> uri;
    Folder hereRoot;
    FragmentHomeBinding fhb;
    HomeFragRecyclerAdaptor adaptor;

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Log.d("amir", getArguments().toString());
//        int[] uriArr = HomeFragmentArgs.fromBundle(getArguments()).getUri();

        int[] uriArr;
        if (getArguments().keySet().contains("uri")) {
            uriArr = HomeFragmentArgs.fromBundle(getArguments()).getUri();
        } else {
            uriArr = new int[0];
        }
        uri = Arrays.stream(uriArr).boxed().collect(Collectors.<Integer>toList());
        hereRoot = InMemoryDatabase.getMyRoot(uri);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_home, container, false);
        fhb = FragmentHomeBinding.bind(frag);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        fhb.homeRecycler.setLayoutManager(layoutManager);
        adaptor = new HomeFragRecyclerAdaptor(uri);
        fhb.homeRecycler.setAdapter(adaptor);

        setupFab();
        return frag;
    }

    static final int folderId = View.generateViewId();
    static final int noteId = View.generateViewId();

    private void setupFab() {
        final SpeedDialView speedDial = fhb.speedDial;
        speedDial.addActionItem(
                new SpeedDialActionItem.Builder(folderId, R.drawable.ic_folder)
                        .setFabBackgroundColor(getResources().getColor(R.color.folder_fab, null))
                        .create()
        );
        speedDial.addActionItem(
                new SpeedDialActionItem.Builder(noteId, R.drawable.ic_note)
                        .setFabBackgroundColor(getResources().getColor(R.color.ans_fab, null))
                        .create()
        );
        speedDial.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem actionItem) {
                NavController nc = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);
                int id = actionItem.getId();
                if (id == folderId) {
                    showCreateNewFolderDialog();
                    speedDial.close();
                    return true;
                } else if (id == noteId) {
                    showCreateNewAnswerSheetDialog();
                    speedDial.close();
                    return true;
                } else {
                    throw new IllegalArgumentException("no such fab id");
                }
            }
        });
    }

    public void showCreateNewAnswerSheetDialog() {
        FragmentManager fm = getChildFragmentManager();
        NewAnswerSheetDialogFragment newAnswerSheetDialogFragment = NewAnswerSheetDialogFragment
                .newInstance();
        newAnswerSheetDialogFragment.show(fm, "fragment_new_answer_sheet");
    }

    @Override
    public void onFinishCreateAnswerSheet(AnswerSheet newAnswerSheet) {
        Toast.makeText(getContext(), String.format(
                "New answer sheet %s created form %d to %d",
                newAnswerSheet.getName(),
                newAnswerSheet.getFirstIndex(),
                newAnswerSheet.getLastIndex()
                ),
                Toast.LENGTH_LONG
        ).show();
        hereRoot.getAnswerSheets().add(newAnswerSheet);
        adaptor.notifyDataSetChanged();
    }

    public void showCreateNewFolderDialog() {
        FragmentManager fm = getChildFragmentManager();
        NewFolderDialogFragment newAnswerSheetDialogFragment = NewFolderDialogFragment
                .newInstance();
        newAnswerSheetDialogFragment.show(fm, "fragment_new_folder");
    }

    @Override
    public void onFinishCreateFolder(Folder newFolder) {
        Toast.makeText(getContext(), String.format(
                "New folder %s created",
                newFolder.getName()
                ),
                Toast.LENGTH_LONG
        ).show();
        hereRoot.getSubFolders().add(newFolder);
        adaptor.notifyDataSetChanged();
    }
}