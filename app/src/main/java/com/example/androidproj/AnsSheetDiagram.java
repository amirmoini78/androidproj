package com.example.androidproj;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.androidproj.databinding.FragmentAnswerSheetDiagramBinding;
import com.example.androidproj.databinding.FragmentDiagramHeaderBinding;
import com.example.androidproj.model.Result;

public class AnsSheetDiagram extends Fragment {

    FragmentAnswerSheetDiagramBinding binding;
    Result result;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        result = AnsSheetDiagramArgs.fromBundle(getArguments()).getResult();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View answerSheetDiagramFragment = inflater.inflate
                (R.layout.fragment_answer_sheet_diagram, container, false);

        binding = FragmentAnswerSheetDiagramBinding.
                bind(answerSheetDiagramFragment);

        HeaderFragment headerFragment = (HeaderFragment)
                getChildFragmentManager().findFragmentById(R.id.fragment_answer_sheet_header);
        PieChartDiagramFragment pieChartDiagramFragment = (PieChartDiagramFragment)
                getChildFragmentManager().findFragmentById(R.id.fragment_answer_sheet_pie_chart);
        headerFragment.setData(result);
        pieChartDiagramFragment.setData(result);

        return answerSheetDiagramFragment;
    }
}