package com.example.androidproj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproj.adaptor.KeysFragmentRecyclerViewAdapter;
import com.example.androidproj.model.InMemoryDatabase;

public class KeysFragment extends Fragment {

    // we can use onCreate if we want to use bundle but in this case we don't want

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // inflate fragment
        View recyclerViewFragment = inflater.inflate(R.layout.fragment_recycler_view, container, false); //should attach to root be false?
        // find the fragment's recycler view
        RecyclerView recyclerView = recyclerViewFragment.findViewById(R.id.fragmentRecyclerView);
        // instantiate and set a layout manager for recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        // instantiate and set an adapter for recycler view
        KeysFragmentRecyclerViewAdapter keysFragmentRecyclerViewAdapter =
                new KeysFragmentRecyclerViewAdapter(InMemoryDatabase.tempAnswerSheet);
        recyclerView.setAdapter(keysFragmentRecyclerViewAdapter);
        // return the fragment
        return recyclerViewFragment;
    }
}
