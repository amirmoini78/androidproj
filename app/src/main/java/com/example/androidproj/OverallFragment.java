package com.example.androidproj;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.androidproj.databinding.FragmentOverallBinding;
import com.example.androidproj.model.InMemoryDatabase;

public class OverallFragment extends Fragment {
    FragmentOverallBinding binding;

    public OverallFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_overall, container, false);
        binding = FragmentOverallBinding.bind(frag);

        BarchartFrag barchartFrag = (BarchartFrag) getChildFragmentManager()
                .findFragmentById(R.id.overall_barchar_frag);
        HeaderFragment headerFragment = (HeaderFragment)
                getChildFragmentManager().findFragmentById(R.id.fragment_overall_header);
        PieChartDiagramFragment pieChartDiagramFragment = (PieChartDiagramFragment)
                getChildFragmentManager().findFragmentById(R.id.fragment_overall_pie_chart);

        barchartFrag.setData(InMemoryDatabase.getOverallResults());
        headerFragment.setData(InMemoryDatabase.rootFolder.getResult());
        pieChartDiagramFragment.setData(InMemoryDatabase.rootFolder.getResult());

        return frag;
    }
}