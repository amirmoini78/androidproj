package com.example.androidproj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.androidproj.model.Folder;

public class NewFolderDialogFragment extends DialogFragment {
    private EditText nameEditText;
    private Button createButton;
    private Button cancelButton;

    public interface NewFolderDialogListener {
        void onFinishCreateFolder(Folder newFolder);
    }

    private NewFolderDialogFragment() {}

    public static NewFolderDialogFragment newInstance() {
        // if you want to have any special argument you should give it here and pass it to bundle.
        return new NewFolderDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setStyle(DialogFragment.STYLE_NORMAL,0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_new_folder, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // find view elements
        nameEditText = view.findViewById(R.id.editTextNewFolderName);
        createButton = view.findViewById(R.id.buttonCreateNewFolder);
        cancelButton = view.findViewById(R.id.buttonCancelCreateNewFolder);

        // set the basic focus on nameEditText and open keyboard
        nameEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // set title
        getDialog().setTitle("Create New Folder");

        //set action for dialog
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEditText.getText().toString();
                ((NewFolderDialogListener) getParentFragment()).onFinishCreateFolder(new Folder(name));
                dismiss();
            }
        });

        // active cancel button
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
