package com.example.androidproj;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.example.androidproj.model.Folder;
import com.example.androidproj.model.InMemoryDatabase;
import com.google.gson.Gson;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


        // Parse Initialize
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("myAppId")
                .server("http://localhost:1337/parse/")
                .build()
        );

        ParseObject data = new ParseObject("StuData");
        Gson gson = new Gson();
        data.put("data",  "teeest");
        data.saveInBackground(); // server storage

        String objectID = data.getObjectId();


//        ParseObject data = new ParseObject("data");
//        Gson gson = new Gson();
//        data.put("value",  "xxxxx");
//        data.pinInBackground(); // Local storage
////        data.saveInBackground(); // server storage
//        String objectID = data.getObjectId();
//
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("data");
//        query.fromLocalDatastore(); // if you want to retrieve from local data store
//        query.getInBackground(objectID, new GetCallback<ParseObject>() {
//            public void done(ParseObject object, ParseException e) {
//                if (e == null) {
//                    Gson gson = new Gson();
//                    Log.d("ALI", object.getString("value"));
//                } else {
//                    Toast.makeText(
//                            getBaseContext(),
//                            "You didn't saved any data",
//                            Toast.LENGTH_LONG
//                    ).show();
//                }
//            }
//        });
    }

}
