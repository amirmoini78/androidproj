package com.example.androidproj;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.service.autofill.Dataset;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.androidproj.databinding.FragmentBarchartBinding;
import com.example.androidproj.model.Result;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BarchartFrag extends Fragment {
    FragmentBarchartBinding binding;

    public BarchartFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View frag = inflater.inflate(R.layout.fragment_barchart, container, false);
        binding = FragmentBarchartBinding.bind(frag);

        return frag;
    }

    public void setData(final HashMap<String, Result> data) {
        Log.d("amir",data.toString());
        BarChart barChart = binding.barchart;

        List<BarEntry> trueEntries = new ArrayList<>();
        List<BarEntry> falseEntries = new ArrayList<>();
        List<BarEntry> noAnsEntries = new ArrayList<>();


        int i = 0;
        for (Result result : data.values()) {
            trueEntries.add(new BarEntry(i, result.withKeyNotAnswered));
            falseEntries.add(new BarEntry(i, result.withKeyTrue));
            noAnsEntries.add(new BarEntry(i, result.withKeyFalse));

            i+=1;
        }

        BarDataSet trueDataset = new BarDataSet(trueEntries,"True");
        BarDataSet falseDataset = new BarDataSet(falseEntries,"False");
        BarDataSet noAnswerDataset = new BarDataSet(noAnsEntries,"No Answer");

        trueDataset.setColor(getResources().getColor(R.color.true_bar,null));
        falseDataset.setColor(getResources().getColor(R.color.false_bar,null));
        noAnswerDataset.setColor(getResources().getColor(R.color.no_ans_bar,null));

        float groupSpace = 0.22f;
        float barSpace = 0.01f;
        float barWidth = 0.25f;

        BarData chartData = new BarData(trueDataset,falseDataset,noAnswerDataset);
        chartData.setBarWidth(barWidth);
        barChart.setData(chartData);
        barChart.groupBars(0, groupSpace, barSpace);
        barChart.setDescription(null);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setCenterAxisLabels(true);
        xAxis.setAxisMinimum(0);
        xAxis.setAxisMaximum(data.size());
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                if((int)value < 0 || (int)value >= data.size()){
                    return "";
                }

                return new ArrayList<>(data.keySet()).get((int)value);
            }
        });

        YAxis yAxisR = barChart.getAxisRight();
        yAxisR.setEnabled(false);

        YAxis yAxisL = barChart.getAxisLeft();
        yAxisL.setDrawGridLines(false);
        yAxisL.setGranularity(1f);
        yAxisL.setGranularityEnabled(true);

        barChart.invalidate();
    }
}