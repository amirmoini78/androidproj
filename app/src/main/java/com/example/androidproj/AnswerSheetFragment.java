package com.example.androidproj;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproj.adaptor.AnswerSheetFragmentRecyclerViewAdapter;
import com.example.androidproj.model.AnswerSheet;
import com.example.androidproj.model.InMemoryDatabase;

public class AnswerSheetFragment extends Fragment {

    // we can use onCreate if we want to use bundle but in this case we don't want

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // inflate the fragment
        View recyclerViewFragment = inflater.inflate(R.layout.fragment_recycler_view, container, false); //should attach to root be false?
        // find the fragment's recycler view
        RecyclerView recyclerView = recyclerViewFragment.findViewById(R.id.fragmentRecyclerView);
        // instantiate and set a layout manager for recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // instantiate and set an adapter for recycler view
        AnswerSheetFragmentRecyclerViewAdapter answerSheetFragmentRecyclerViewAdapter =
                new AnswerSheetFragmentRecyclerViewAdapter(InMemoryDatabase.tempAnswerSheet);
        recyclerView.setAdapter(answerSheetFragmentRecyclerViewAdapter);
        // return the fragment
        return recyclerViewFragment;
    }
}
