package com.example.androidproj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.androidproj.databinding.DialogFragmentNewAnswerSheetBinding;
import com.example.androidproj.model.AnswerSheet;

public class NewAnswerSheetDialogFragment extends DialogFragment {

    public interface NewAnswerSheetDialogListener {
        void onFinishCreateAnswerSheet(AnswerSheet newAnswerSheet);
    }

    private NewAnswerSheetDialogFragment() {}

    public static NewAnswerSheetDialogFragment newInstance() {
        // if you want to have any special argument you should give it here and pass it to bundle.
        return new NewAnswerSheetDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setStyle(DialogFragment.STYLE_NORMAL,0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_new_answer_sheet, container);
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DialogFragmentNewAnswerSheetBinding binding = DialogFragmentNewAnswerSheetBinding.bind(view);
        final EditText firstIndexEditText = binding.editTextNewAnswerSheetFirstIndex;
        final EditText lastIndexEditText = binding.editTextNewAnswerSheetLastIndex;
        final EditText nameEditText = binding.editTextNewAswerSheetName;
        final Button createButton = binding.buttonCreateNewAnswerChart;
        final Button cancelButton = binding.buttonCancelCreateNewAnswerChart;

        // find view elements
//        firstIndexEditText = view.findViewById(R.id.editTextNewAnswerSheetFirstIndex);
//        lastIndexEditText = view.findViewById(R.id.editTextNewAnswerSheetLastIndex);
//        nameEditText = view.findViewById(R.id.editTextNewAswerSheetName);
//        createButton = view.findViewById(R.id.buttonCreateNewAnswerChart);
//        cancelButton = view.findViewById(R.id.buttonCancelCreateNewAnswerChart);

        // set the basic focus on firstIndexEditText and open keyboard
        firstIndexEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // set title
        getDialog().setTitle("Create New Answer Sheet");

        //set action for dialog
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int firstIndex = Integer.parseInt(firstIndexEditText.getText().toString());
                    int lastIndex = Integer.parseInt(lastIndexEditText.getText().toString());
                    String name = nameEditText.getText().toString();
                    if (firstIndex <= lastIndex) {
                        ((NewAnswerSheetDialogListener) getParentFragment()).onFinishCreateAnswerSheet
                                (new AnswerSheet(firstIndex, lastIndex, name));
                        dismiss();
                    } else {
                        firstIndexEditText.setText(String.valueOf(lastIndex));
                        lastIndexEditText.setText(String.valueOf(firstIndex));
                        Toast.makeText(
                                getContext(),
                                "Last index should be bigger or equal to first\ntry again",
                                Toast.LENGTH_LONG
                        ).show();
                    }
                } catch (ClassCastException e) {
                    firstIndexEditText.getText().clear();
                    lastIndexEditText.getText().clear();
                    Toast.makeText(
                            getContext(),
                            "Your input value should be number in first two text boxes",
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        });

        // active cancel button
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
