package com.example.androidproj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.androidproj.databinding.FragmentDiagramHeaderBinding;
import com.example.androidproj.model.Result;

public class HeaderFragment extends Fragment {

    FragmentDiagramHeaderBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View diagramHeaderFragment = inflater.inflate
                (R.layout.fragment_diagram_header, container, false);

        binding = FragmentDiagramHeaderBinding.
                bind(diagramHeaderFragment);

        return diagramHeaderFragment;
    }

    public void setData(Result result) {
        binding.textViewTrue.setText(String.valueOf(result.withKeyTrue));
        binding.textViewFalse.setText(String.valueOf(result.withKeyFalse));
        binding.textViewWithKeyNotAnswered.setText(String.valueOf(result.withKeyNotAnswered));
        binding.textViewWithOutKeyAnswered.setText(String.valueOf(result.withOutKeyAnswered));
        binding.textViewWithOutKeyNotAnswered.setText(String.valueOf(result.withOutKeyNotAnswered));
    }
}
