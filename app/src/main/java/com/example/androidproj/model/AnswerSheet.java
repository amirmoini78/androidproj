package com.example.androidproj.model;

import java.io.Serializable;

public class AnswerSheet implements Serializable {

    private int firstIndex;
    private int lastIndex;
    private String name;
    private int[] answers;
    private int[] keys;

    public AnswerSheet(int firstIndex, int lastIndex, String name) {
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
        this.name = name;
        answers = new int[lastIndex - firstIndex + 1];
        keys = new int[lastIndex - firstIndex + 1];
    }

    public int getFirstIndex() {
        return firstIndex;
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public String getName() {
        return name;
    }

    public int[] getAnswers() {
        return answers;
    }

    public int[] getKeys() {
        return keys;
    }

    public Result getResult() {
        Result result = new Result();
        for (int i = 0; i < answers.length; i++) {
            if (keys[i] != 0) {
                if (keys[i] == answers[i]) {
                    result.withKeyTrue ++;
                } else if (answers[i] != 0) {
                    result.withKeyFalse ++;
                } else {
                    result.withKeyNotAnswered ++;
                }
            } else {
                if (answers[i] != 0) {
                    result.withOutKeyAnswered ++;
                } else {
                    result.withOutKeyNotAnswered ++;
                }
            }
        }
        return result;
    }
}
