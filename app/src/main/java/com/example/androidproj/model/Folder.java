package com.example.androidproj.model;

import java.util.ArrayList;
import java.util.List;

public class Folder {
    private String name;
    private List<Folder> subFolders;
    private List<AnswerSheet> answerSheets;

    public Folder(String name) {
        this.name = name;
        subFolders = new ArrayList<>();
        answerSheets = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Folder> getSubFolders() {
        return subFolders;
    }

    public List<AnswerSheet> getAnswerSheets() {
        return answerSheets;
    }

    public int getTotalCount(){
        return subFolders.size() + answerSheets.size();
    }

    public Result getResult() {
        Result result = new Result();
        for (int i = 0; i < answerSheets.size(); i++) {
            result.add(answerSheets.get(i).getResult());
        }
        for (int i = 0; i < subFolders.size(); i++) {
            result.add(subFolders.get(i).getResult());
        }
        return result;
    }
}
