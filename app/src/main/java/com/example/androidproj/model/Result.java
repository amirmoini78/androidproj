package com.example.androidproj.model;

import java.io.Serializable;

public class Result implements Serializable {
    public int withKeyTrue;
    public int withKeyFalse;
    public int withKeyNotAnswered;
    public int withOutKeyAnswered;
    public int withOutKeyNotAnswered;

    public void add(Result result) {
        this.withKeyTrue += result.withKeyTrue;
        this.withKeyFalse += result.withKeyFalse;
        this.withKeyNotAnswered += result.withKeyNotAnswered;
        this.withOutKeyAnswered += result.withOutKeyAnswered;
        this.withOutKeyNotAnswered += result.withOutKeyNotAnswered;
    }

}
