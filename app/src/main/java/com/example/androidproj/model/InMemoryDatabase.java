package com.example.androidproj.model;

import androidx.navigation.NavController;

import com.example.androidproj.HomeFragmentDirections;

import java.util.HashMap;
import java.util.List;

public class InMemoryDatabase {

    // this parameter is using for transfer data between Main Activity and Fragments
    public static AnswerSheet tempAnswerSheet;

    public static Folder rootFolder = new Folder("root");
    static {
        Folder f1 = new Folder("folder1");
        f1.getSubFolders().add(new Folder("subFolder1"));
        AnswerSheet ash1 = new AnswerSheet(1,1119,"ans sheet sub1");
        ash1.getAnswers()[0] = 1;
        ash1.getAnswers()[1] = 2;
        ash1.getAnswers()[2] = 0;
        ash1.getKeys()[0] = 1;
        ash1.getKeys()[1] = 1;
        ash1.getKeys()[2] = 2;
        f1.getAnswerSheets().add(ash1);
        rootFolder.getSubFolders().add(f1);
        Folder f2 = new Folder("folder2");
        AnswerSheet ash2 = new AnswerSheet(3,40,"shit");
        ash2.getKeys()[0] = 1;
        ash2.getKeys()[1] = 2;
        ash2.getKeys()[2] = 3;
        ash2.getKeys()[3] = 4;
        ash2.getAnswers()[0] = 1;
        ash2.getAnswers()[1] = 2;
        ash2.getAnswers()[2] = 0;
        ash2.getAnswers()[3] = 1;
        f2.getAnswerSheets().add(ash2);
        rootFolder.getSubFolders().add(f2);
        Folder f3 = new Folder("folder3");
        AnswerSheet ash3 = new AnswerSheet(3,40,"sheeet");
        ash3.getKeys()[0] = 1;
        ash3.getKeys()[1] = 2;
        ash3.getKeys()[2] = 3;
        ash3.getKeys()[3] = 4;
        ash3.getAnswers()[0] = 1;
        ash3.getAnswers()[1] = 2;
        ash3.getAnswers()[2] = 0;
        ash3.getAnswers()[3] = 1;
        f3.getAnswerSheets().add(ash3);
        rootFolder.getSubFolders().add(f3);
        rootFolder.getAnswerSheets().add(new AnswerSheet(222,3131,"sheet1"));
    }

    public static Folder getMyRoot(List<Integer> uri){
        Folder hisFolder = rootFolder;
        for(int i:uri){
            hisFolder = hisFolder.getSubFolders().get(i);
        }
        return hisFolder;
    }

    public static HashMap<String, Result> getOverallResults() {
        HashMap<String, Result> results = new HashMap<>();
        for (int i = 0; i < rootFolder.getSubFolders().size(); i++) {
            results.put(rootFolder.getSubFolders().get(i).getName(),
                    rootFolder.getSubFolders().get(i).getResult());
        }
        return results;
    }
}
