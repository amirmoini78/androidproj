package com.example.androidproj;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.example.androidproj.R;
import com.example.androidproj.adaptor.AnswerSheetTabLayoutFragmentViewPager2Adapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class AnswerSheetTabLayoutWithViewPager2Fragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // inflate fragment
        View tabLayoutFragment = inflater.inflate
                (R.layout.fragment_tab_layout_answer_sheet_and_keys, container, false); // should attach to root be false?
        // find tab layout and view pager 2 by id
        TabLayout tabLayout = tabLayoutFragment.findViewById(R.id.tabLayoutAnswerAndKey);
        ViewPager2 viewPager2 = tabLayoutFragment.findViewById(R.id.viewPagerAnswerAndKey);
        // instantiate view pager 2 Adapter (FragmentStateAdapter),
        // sub fragments and set it for view pager
        AnswerSheetTabLayoutFragmentViewPager2Adapter viewPager2Adapter =
                new AnswerSheetTabLayoutFragmentViewPager2Adapter(getActivity()); // maybe it doesn't work well?
        viewPager2Adapter.addFragment(new AnswerSheetFragment());
        viewPager2Adapter.addFragment(new KeysFragment());
        viewPager2.setAdapter(viewPager2Adapter);
        // instantiate a TabLayoutMediator witch integrate tab layout with view pager 2 instead of
        // tabLayout.setupWithViewPager(viewPager) witch integrate tab layout with view pager
        new TabLayoutMediator(tabLayout, viewPager2,
                new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0: {
                        tab.setText("Answer Sheet");
                        break;
                    }
                    case 1: {
                        tab.setText("Keys");
                    }
                }
            }
        }).attach();
        // return fragment
        return tabLayoutFragment;
    }

}
