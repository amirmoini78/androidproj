package com.example.androidproj;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.androidproj.databinding.ActivityMainBinding;
import com.example.androidproj.model.Folder;
import com.example.androidproj.model.InMemoryDatabase;
import com.google.android.material.navigation.NavigationView;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.google.gson.Gson;
import com.parse.ParseQuery;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity  {

    AppBarConfiguration appBarConfiguration;
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

//        navController.setGraph(R.navigation.nav_graph, HomeFragmentDirections.homeSelfAction(new int[0]).getArguments());

        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph())
                .setOpenableLayout(binding.drawerLayout).build();

        setupNavigationMenu(navController);
        setupActionBar(navController);
    }



    private void setupNavigationMenu(NavController navController) {
        NavigationView nv = binding.drawerView;
        NavigationUI.setupWithNavController(nv, navController);
        MenuItem saveBut = binding.drawerView.getMenu().findItem(R.id.drawer_save);
        MenuItem restoreBut = binding.drawerView.getMenu().findItem(R.id.drawer_restore);
        saveBut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                save();
                return true;
            }
        });
        restoreBut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                restore();
                return true;
            }
        });
    }

    private void setupActionBar(NavController navController) {
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_actionbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.sth:
            //dos sth
//                break;
            default:
                NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
                return NavigationUI.onNavDestinationSelected(item, navController)
                        || super.onOptionsItemSelected(item);

        }
//        return true;
    }

    private String objectID;

    public void save() {
        ParseObject data = new ParseObject("StuData");
        Gson gson = new Gson();
        data.put("data",  gson.toJson(InMemoryDatabase.rootFolder));
//        data.pinInBackground(); // Local storage
        data.saveInBackground(); // server storage
        objectID = data.getObjectId();
    }

    public void restore() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("data");
//        query.fromLocalDatastore(); // if you want to retrieve from local data store
        query.getInBackground(objectID, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    Gson gson = new Gson();
                    Log.d("ALI", object.getString("value"));
                    InMemoryDatabase.rootFolder =
                            gson.fromJson(object.getString("value"), Folder.class);
                } else {
                    Toast.makeText(
                            getBaseContext(),
                            "You didn't saved any data",
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        });
    }
}