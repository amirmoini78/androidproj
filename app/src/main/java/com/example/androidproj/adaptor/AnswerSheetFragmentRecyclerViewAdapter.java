package com.example.androidproj.adaptor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproj.R;
import com.example.androidproj.databinding.ElementTestBinding;
import com.example.androidproj.model.AnswerSheet;

public class AnswerSheetFragmentRecyclerViewAdapter extends RecyclerView.Adapter
        <AnswerSheetFragmentRecyclerViewAdapter.TestViewHolder> {

    private AnswerSheet answerSheet;

    public AnswerSheetFragmentRecyclerViewAdapter(AnswerSheet answerSheet) {
        this.answerSheet = answerSheet;
        Log.d("Ali", "answer sheet in creating frag");
    }

    @NonNull
    @Override
    public TestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View testLayoutElement = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.element_test, viewGroup, false); //should attach to root be false?
        return new TestViewHolder(testLayoutElement);
    }

    @Override
    public void onBindViewHolder(@NonNull final TestViewHolder testViewHolder, final int i) {
        testViewHolder.elementTestBinding.textViewTestNumber
                .setText(String.valueOf(i + answerSheet.getFirstIndex()));

        switch (answerSheet.getAnswers()[i]) {
            case 1: {
                testViewHolder.elementTestBinding.radioGroupTestOptions.
                        check(R.id.radioButtonTestOptionOne);
                break;
            }
            case 2: {
                testViewHolder.elementTestBinding.radioGroupTestOptions.
                        check(R.id.radioButtonTestOptionTwo);
                break;
            }
            case 3: {
                testViewHolder.elementTestBinding.radioGroupTestOptions.
                        check(R.id.radioButtonTestOptionThree);
                break;
            }
            case 4: {
                testViewHolder.elementTestBinding.radioGroupTestOptions.
                        check(R.id.radioButtonTestOptionFour);
                break;
            }
            default: {
                testViewHolder.elementTestBinding.radioGroupTestOptions.clearCheck();
            }
        }

        testViewHolder.elementTestBinding.radioButtonTestOptionOne.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getAnswers()[i] == 1) {
                    testViewHolder.elementTestBinding.radioGroupTestOptions.clearCheck();
                    answerSheet.getAnswers()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getAnswers()[i] = 1;
                }
            }
        });
        testViewHolder.elementTestBinding.radioButtonTestOptionTwo.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getAnswers()[i] == 2) {
                    testViewHolder.elementTestBinding.radioGroupTestOptions.clearCheck();
                    answerSheet.getAnswers()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getAnswers()[i] = 2;
                }
            }
        });
        testViewHolder.elementTestBinding.radioButtonTestOptionThree.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getAnswers()[i] == 3) {
                    testViewHolder.elementTestBinding.radioGroupTestOptions.clearCheck();
                    answerSheet.getAnswers()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getAnswers()[i] = 3;
                }
            }
        });
        testViewHolder.elementTestBinding.radioButtonTestOptionFour.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getAnswers()[i] == 4) {
                    testViewHolder.elementTestBinding.radioGroupTestOptions.clearCheck();
                    answerSheet.getAnswers()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getAnswers()[i] = 4;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d("Ali", "answer sheet in onBind of frag" + answerSheet.getAnswers().length + answerSheet.getName());
        return answerSheet.getAnswers().length;
    }

    static class TestViewHolder extends RecyclerView.ViewHolder {
        ElementTestBinding elementTestBinding;

        public TestViewHolder(@NonNull View itemView) {
            super(itemView);
            elementTestBinding = ElementTestBinding.bind(itemView);
        }
    }
}
