package com.example.androidproj.adaptor;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproj.R;
import com.example.androidproj.databinding.ElementKeyBinding;
import com.example.androidproj.model.AnswerSheet;

public class KeysFragmentRecyclerViewAdapter extends RecyclerView.Adapter
        <KeysFragmentRecyclerViewAdapter.KeyViewHolder> {

    AnswerSheet answerSheet;

    public KeysFragmentRecyclerViewAdapter(AnswerSheet answerSheet) {
        this.answerSheet = answerSheet;
    }

    @NonNull
    @Override
    public KeyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View testLayoutElement = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.element_key, viewGroup, false); // should attach to root be false?
        return new KeyViewHolder(testLayoutElement);
    }

    @Override
    public void onBindViewHolder(@NonNull final KeyViewHolder keyViewHolder, final int i) {
        keyViewHolder.elementKeyBinding.textViewKeyNumber.
                setText(String.valueOf(i + answerSheet.getFirstIndex()));

        switch (answerSheet.getKeys()[i]) {
            case 1: {
                keyViewHolder.elementKeyBinding.radioGroupKeyOptions.
                        check(R.id.radioButtonKeyOptionOne);
                break;
            }
            case 2: {
                keyViewHolder.elementKeyBinding.radioGroupKeyOptions.
                        check(R.id.radioButtonKeyOptionTwo);
                break;
            }
            case 3: {
                keyViewHolder.elementKeyBinding.radioGroupKeyOptions.
                        check(R.id.radioButtonKeyOptionThree);
                break;
            }
            case 4: {
                keyViewHolder.elementKeyBinding.radioGroupKeyOptions.
                        check(R.id.radioButtonKeyOptionFour);
                break;
            }
            default: {
                keyViewHolder.elementKeyBinding.radioGroupKeyOptions.clearCheck();
            }
        }

        keyViewHolder.elementKeyBinding.radioButtonKeyOptionOne.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getKeys()[i] == 1) {
                    keyViewHolder.elementKeyBinding.radioGroupKeyOptions.clearCheck();
                    answerSheet.getKeys()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getKeys()[i] = 1;
                }
            }
        });
        keyViewHolder.elementKeyBinding.radioButtonKeyOptionTwo.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getKeys()[i] == 2) {
                    keyViewHolder.elementKeyBinding.radioGroupKeyOptions.clearCheck();
                    answerSheet.getKeys()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getKeys()[i] = 2;
                }
            }
        });
        keyViewHolder.elementKeyBinding.radioButtonKeyOptionThree.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getKeys()[i] == 3) {
                    keyViewHolder.elementKeyBinding.radioGroupKeyOptions.clearCheck();
                    answerSheet.getKeys()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getKeys()[i] = 3;
                }
            }
        });
        keyViewHolder.elementKeyBinding.radioButtonKeyOptionFour.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton = (RadioButton) v;
                if (answerSheet.getKeys()[i] == 4) {
                    keyViewHolder.elementKeyBinding.radioGroupKeyOptions.clearCheck();
                    answerSheet.getKeys()[i] = 0;
                } else {
                    radioButton.setChecked(true);
                    answerSheet.getKeys()[i] = 4;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return answerSheet.getKeys().length;
    }

    static class KeyViewHolder extends RecyclerView.ViewHolder {
        ElementKeyBinding elementKeyBinding;

        public KeyViewHolder(@NonNull View itemView) {
            super(itemView);
            elementKeyBinding = ElementKeyBinding.bind(itemView);
        }
    }
}
