package com.example.androidproj.adaptor;

import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidproj.HomeFragmentDirections;
import com.example.androidproj.MainActivity;
import com.example.androidproj.R;
import com.example.androidproj.databinding.AnsRowBinding;
import com.example.androidproj.databinding.FolderRowBinding;
import com.example.androidproj.model.AnswerSheet;
import com.example.androidproj.model.Folder;
import com.example.androidproj.model.InMemoryDatabase;

import java.util.List;

enum RowType {
    FOLDER, ANS
}

public class HomeFragRecyclerAdaptor extends RecyclerView.Adapter<HomeFragRecyclerAdaptor.RowViewHolder> {
    private Folder hereRootFolder;
    private List<Integer> uri;

    public HomeFragRecyclerAdaptor(List<Integer> uri) {
        this.uri = uri;
        this.hereRootFolder = InMemoryDatabase.getMyRoot(uri);
    }

    @Override
    public int getItemViewType(int position) {
        if (position < hereRootFolder.getSubFolders().size()) {
            return RowType.FOLDER.ordinal();
        } else {
            return RowType.ANS.ordinal();
        }
    }

    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View card;
        switch (RowType.values()[viewType]) {
            case FOLDER:
                card = LayoutInflater.from(parent.getContext()).inflate(R.layout.folder_row, parent, false);
                break;
            case ANS:
                card = LayoutInflater.from(parent.getContext()).inflate(R.layout.ans_row, parent, false);
                break;
            default:
                throw new IllegalArgumentException("no such view type.");
        }
        return new RowViewHolder(card);
    }

    @Override
    public void onBindViewHolder(@NonNull final RowViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case 0:
                FolderRowBinding bindingF = FolderRowBinding.bind(holder.itemView);
                bindingF.homeAnsRowTxt.setText(hereRootFolder.getSubFolders().get(position).getName());
                bindingF.homeAnsRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int[] newUri = new int[uri.size() + 1];
                        for (int i = 0; i < uri.size(); i++) {
                            newUri[i] = uri.get(i);
                        }
                        newUri[uri.size()] = position;
                        HomeFragmentDirections.HomeSelfAction action = HomeFragmentDirections.homeSelfAction(newUri);
                        Navigation.findNavController(holder.itemView).navigate(action);
                    }
                });
                holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                        MenuItem Delete = menu.add(R.menu.home_item_context, 1, 1, "Delete");
                        Delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                hereRootFolder.getSubFolders().remove(position);
                                notifyDataSetChanged();
                                return true;
                            }
                        });
                    }
                });
                break;
            case 1:
                AnsRowBinding bindingA = AnsRowBinding.bind(holder.itemView);
                final AnswerSheet ash = hereRootFolder.getAnswerSheets()
                        .get(position - hereRootFolder.getSubFolders().size());
                bindingA.homeAnsRowTxt.setText(ash.getName());
                bindingA.ansEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InMemoryDatabase.tempAnswerSheet = ash;
                        Navigation.findNavController(holder.itemView)
                                .navigate(HomeFragmentDirections.homeToAnswerChartNavigationAction());
                    }
                });
                bindingA.ansDiag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Navigation.findNavController(holder.itemView)
                                .navigate(HomeFragmentDirections.homeAnswerSheetDiagramAction(ash.getResult()));
                    }
                });
                holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                    @Override
                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                        MenuItem Delete = menu.add(R.menu.home_item_context, 1, 1, "Delete");
                        Delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                hereRootFolder.getAnswerSheets().remove(ash);
                                notifyDataSetChanged();
                                return true;
                            }
                        });
                    }
                });
                break;
            default:
                throw new IllegalArgumentException("no such view type");
        }
    }

    @Override
    public int getItemCount() {
        return hereRootFolder.getTotalCount();
    }

    public static class RowViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }
}
