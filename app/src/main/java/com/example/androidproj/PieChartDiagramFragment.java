package com.example.androidproj;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.androidproj.databinding.FragmentPieChartDiagramBinding;
import com.example.androidproj.model.Result;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

public class PieChartDiagramFragment extends Fragment {

    FragmentPieChartDiagramBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View pieChartDiagramFragment = inflater.inflate
                (R.layout.fragment_pie_chart_diagram, container, false);

        binding = FragmentPieChartDiagramBinding.
                bind(pieChartDiagramFragment);

        return pieChartDiagramFragment;
    }

    public void setData(Result result) {
        List<PieEntry> entries = new ArrayList<>();
        int withKeySum = result.withKeyTrue + result.withKeyFalse + result.withKeyNotAnswered;
        float truePercent = result.withKeyTrue / ((float) withKeySum) * 100;
        float falsePercent = result.withKeyFalse / ((float) withKeySum) * 100;
        float notAnsweredPercent = result.withKeyNotAnswered / ((float) withKeySum) * 100;
        entries.add(new PieEntry(result.withKeyTrue,
                result.withKeyTrue == 0 ? "" : String.format("%%%.02f", truePercent)));
        entries.add(new PieEntry(result.withKeyFalse,
                result.withKeyFalse == 0 ? "" : String.format("%%%.02f", falsePercent)));
        entries.add(new PieEntry(result.withKeyNotAnswered,
                result.withKeyNotAnswered == 0 ? "" : String.format("%%%.02f", notAnsweredPercent)));
        PieDataSet set = new PieDataSet(entries, "Basic Results");
        set.setColors(
                getResources().getColor(R.color.true_bar, null),
                getResources().getColor(R.color.false_bar, null),
                getResources().getColor(R.color.no_ans_bar, null)
        );
        set.setValueTextSize(0);
        PieData data = new PieData(set);
        binding.pieChart.setData(data);
        binding.pieChart.setDescription(null);
        binding.pieChart.invalidate(); // refresh
    }
}
