# MultiChoice helper
An app which helps students who deal with lots of multichoice questions. They can categorize their choice sheets and grade them automatically by entering each question's correct answer.
It was developed as the project for Android development course taken by the developers!
An unordered roadmap is:
* Image recognition to scan the correct answers from answer sheets found in many books.
* Improve the visuals of the choices page
* Add dark mod because battery is an issue for students who study long hours
